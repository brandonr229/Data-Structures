#include "stdafx.h"
#include "LLStack.h"

LLStack::LLStack()
{
	count = 0;
	LL = new LList();
}


LLStack::~LLStack()
{

}

void LLStack::Push(int value)
{
	LL->AddNode(value);
	 count++;
}

void LLStack::Pop()
{
	LLNode * temp = LL->GetHeadNode();
		if (temp != NULL) {
			LL->RemoveFirst();
			count--;
		}
}

int LLStack::GetLength()
{
	return count;
}

void LLStack::PrintAll()
{
	LL->PrintNodes();
}

int LLStack::Peek()
{
	LLNode * temp = LL->GetHeadNode();
	if (temp != NULL) {
		return temp->value;
	}
	return -99;
}
