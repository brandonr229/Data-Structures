#pragma once

class LLNode
{
public:

	LLNode(int val) { next = NULL; value = val; }
	~LLNode() {}

	LLNode* next;
	int value;
};

