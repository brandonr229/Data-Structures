#pragma once
#include "LList.h"
#include "LLNode.h"

class LLStack
{
public:
	LLStack();
	~LLStack();

	void Push(int value);
	void Pop();
	int Peek();
	int GetLength();
	void PrintAll();

private:
	int count;
	LList * LL;
};

