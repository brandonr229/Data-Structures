// DataStructure.cpp : Defines the entry point for the console application.
//

//Adding a comment for testing

#include "stdafx.h"
#include "LLNode.h"
#include "LList.h"
#include <iostream>
#include "LLStack.h"

using namespace std;


int main()
{


	void LinkedListTest(); {


		LList * LL = new LList();
		LL->AddNode(1);
		LL->AddNode(2);
		LL->AddNode(3);
		LL->AddNode(4);
		LL->AddNode(5);

		cout << "Pre-end Addition\n" << endl;
		LL->PrintNodes();

		cout << "\nPost-end Addition (Adding 6 to end)\n" << endl;
		LL->AddNodeEnd(6); //adds (6) to end of list
		LL->PrintNodes();
		cout << "\nRemove head node\n" << endl;
		LL->RemoveNode(5); //removes the first node (1)
		LL->PrintNodes();

		cout << "\nDeletion of node 4\n" << endl;
		LL->RemoveNode(4);
		LL->PrintNodes();

		cout << "\nDeletion of node 6\n" << endl;
		LL->RemoveNode(6);
		LL->PrintNodes();

		cout << "\nDeletion of node 2\n" << endl;
		LL->RemoveNode(2);
		LL->PrintNodes();

		cout << "\nDeletion of node 1\n" << endl;
		LL->RemoveNode(1);
		LL->PrintNodes();

		cout << "\nDeletion of node 3\n" << endl;
		LL->RemoveNode(3);
		LL->PrintNodes();

		delete LL;
	}
	void LLStackTest(); {
		LLStack * LLS = new LLStack();

		cout << "\nLLS Add to Stack\n" << endl;
		/*LLS->Push(2);
		LLS->Push(16);
		LLS->Push(5);
		LLS->Push(53);
		LLS->Push(NULL); //acts as 0
		LLS->Push(12);
		LLS->Push(-328535545); 
		LLS->Push(-43); //accepts negitive int
		LLS->Push(4395778); //cannot use longs because it's looking for an int*/



		for (int i = 0; i < 1000; i++) //adds 1000 nodes
		{
			LLS->Push(i);
		}
		LLS->PrintAll();

		for (int i = 0; i < 500; i++)
		{
			LLS->Pop();
		}


		cout << "\nLength of Stack Before Removal\n" << endl;
		cout << LLS->GetLength() << endl;

		/*cout << "\nLLS Remove from Stack\n" << endl; //can remove all of the stack and try and remove more, but doesn't break the code
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop();
		LLS->Pop(); 
		LLS->Pop();
		LLS->PrintAll();*/

		cout << "\nLength of Stack After Removal\n" << endl;
		cout << LLS->GetLength() << endl;

		cout << "\nHeader Node Value\n" << endl;
		cout << LLS->Peek() << endl; //if you don't have anything in the stack, outputs -99

		delete LLS;
	}
    return 0;
}

 

/*
	hash tables are a combination of arrays and link lists. 
	they use indexes (like arrays) and then use link lists. 

	binary trees use nodes that are able to point at two other nodes. 
	data that is less than top data is stored to the left. ie 25 -> 22
	data that is greater than top data is stored to the right. 25 -> 27
		root-the node that is the top of the binary tree
		parent- the node that is passing a type to another node
		child- the node that is having data passed to it
		leaf- does not connect to any other nodes (end of tree)

	struct- varriable that stores similar information (does not have to be same type)
	ie char name[20]; int age;

	recursion function- a function that calls itself
	need to be careful not to get in infinte loop. will crash computer.
		base case- ending point of the function. 
	git is a repository
		a repository is a container for a project you want to track
		can have many different repositories for amny different projects on your computer
		like a priject folder which GitHub tracks the contents of for us
		tracks changes made to files that are below it in the folder
		commits- like having multiple save points in a game as you progress. 


*/