#include "stdafx.h"
#include "BinaryTree.h"


BinaryTree::BinaryTree()
{
	root = NULL;
	count = 0;
}


BinaryTree::~BinaryTree()
{
}

void BinaryTree::AddNode(int value)
{
	
		//create node object
		TreeNode* newNode = new TreeNode(value);

		

		if (root == NULL) {
			root = newNode;
		}
		else
		{

			//runner to find place of location
			TreeNode * currentNode = root;
			bool wentLeft = true;

			while (currentNode != NULL)
			{
				if (newNode->value < currentNode->value) {
					if (currentNode->LChild == NULL)
					{
						currentNode->LChild = newNode;
						newNode->Parent = currentNode;
						break;
					}
					else
					{
						currentNode = currentNode->LChild;
					}
				}
				else
				{
					if (currentNode->RChild == NULL)
					{
						currentNode->RChild = newNode;
						newNode->Parent = currentNode;
						break;
					}
					else
					{
						currentNode = currentNode->RChild;
					}
				}
			}
		}

		count++;
	
}

void BinaryTree::RemoveNode(int val)
{
	//remove node(s)
}

void BinaryTree::PrintAll()
{
	//recursive print
	//in-order, pre-order, post-order
	//binary tree traversals
}
