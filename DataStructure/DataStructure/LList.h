#pragma once
#include <iostream>
#include "LLNode.h"

class LList
{

public:
	LList();
	~LList();

	bool AddNodeAfterValue(int newValue, int locationValue);
	void AddNode(int val);
	void AddNodeEnd(int val);
	LLNode * GetHeadNode();
	bool RemoveFirst();
	bool RemoveNode(int val);
	void PrintNodes();
	bool IsEmpty();

	

private:
	LLNode* head;
	int count = 0;

};

