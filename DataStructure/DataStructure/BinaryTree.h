#pragma once
#include "TreeNode.h"

class BinaryTree
{
public:
	BinaryTree();
	~BinaryTree();

	TreeNode *	root;
	int			count = 0;

	void	AddNode(int value);
	void	RemoveNode(int value);
	void	PrintAll();
};

